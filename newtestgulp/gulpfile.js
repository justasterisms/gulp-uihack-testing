var gulp        = require('gulp');
var sass        = require('gulp-sass');
var rename      = require('gulp-rename');
var cssmin      = require('gulp-clean-css');
// var concat      = require('gulp-concat');
var prefix      = require('gulp-autoprefixer');
var browserSync = require('browser-sync');
var reload      = browserSync.reload;
var size        = require('gulp-size');
var scssLint    = require('stylelint-scss');

gulp.task('framework-scss', function() {
  return gulp.src('src/scss/**/*.scss')
    .pipe(sass())
    .pipe(size({ gzip: true, showFiles: true }))
    .pipe(prefix())
    .pipe(gulp.dest('src/css'))
    .pipe(cssmin())
    .pipe(size({ gzip: true, showFiles: true }))
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('src/css'))
    .pipe(gulp.dest('site/css'))
    .pipe(reload({stream:true}));
});

gulp.task('site-scss', function() {
  return gulp.src('site/scss/**/*.scss')
    .pipe(sass())
    .pipe(size({ gzip: true, showFiles: true }))
    .pipe(prefix())
    .pipe(gulp.dest('site/css'))
    .pipe(cssmin())
    .pipe(size({ gzip: true, showFiles: true }))
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('site/css'))
    .pipe(reload({stream:true}));
});

gulp.task('browser-sync', function() {
    browserSync({
        server: {
            baseDir: "site"
        }
    });
});


gulp.task('sass-lint', function () {
  gulp.src('scss/**/*.scss')
    .pipe(scssLint())
    .pipe(scssLint.format())
    .pipe(scssLint.failOnError());
});

gulp.task('watch', function() {
  gulp.watch('src/scss/**/*.scss', ['framework-scss', 'site-scss', 'sass-lint']);
  gulp.watch('site/scss/**/*.scss', ['site-scss', 'sass-lint']);
});


// gulp.task('jshint', function() {
//   gulp.src('js/*.js')
//     .pipe(jshint())
//     .pipe(jshint.reporter('default'));
// });

gulp.task('default', ['browser-sync', 'framework-scss', 'site-scss', 'watch']);